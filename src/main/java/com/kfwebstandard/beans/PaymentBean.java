package com.kfwebstandard.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.enterprise.context.RequestScoped;

import javax.inject.Named;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

/**
 * Backing bean with JSR-303 Bean Validation
 *
 * @author Ken
 */
@Named("payment")
@RequestScoped
public class PaymentBean implements Serializable {

    @DecimalMin("10")
    @DecimalMax("10000")
    private BigDecimal amount;
    @Size(min = 16, message = "{com.kfwebstandard.creditCardLength}")
    @LuhnCheck
    private String card = "";
    @Future
    private Date date = new Date();

    public void setAmount(BigDecimal newValue) {
        amount = newValue;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setCard(String newValue) {
        card = newValue;
    }

    public String getCard() {
        return card;
    }

    public void setDate(Date newValue) {
        date = newValue;
    }

    public Date getDate() {
        return date;
    }
}
